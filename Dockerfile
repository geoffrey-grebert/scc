FROM golang as build

ENV GOOS=linux \
GOARCH=amd64 \
CGO_ENABLED=0

ARG GIT_REPO="boyter/scc"
ARG VERSION
RUN apt-get update && apt-get install -yqqq jq && \
    if [ -z "$VERSION" ]; then \
        VERSION=$( curl -s "https://api.github.com/repos/${GIT_REPO}/releases/latest" | jq -r '.tag_name' ); \
    fi && \
    git clone --depth 1 --branch "${VERSION}" "https://github.com/${GIT_REPO}.git"
WORKDIR /go/scc
RUN go build -ldflags="-s -w"

FROM alpine
COPY --from=build /go/scc/scc /usr/local/bin/scc
COPY scc-ci.sh /usr/local/bin/scc-ci
RUN apk add --no-cache jq  && chmod +x /usr/local/bin/scc-ci
